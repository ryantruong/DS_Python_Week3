# DS_Python_Week3

Week 3 - Recommendation System

## Recommendation system on MovieLens data - Small dataset (100.000 ratings)
## [Click here for Code](Recommendation_System_Small_Data.ipynb)

## Recommendation system on MovieLens data - Large dataset (1.000.000 ratings)
## [Click here for Code](Recommendation_System_1_Million.ipynb)
